
CC = $(CROSS_COMPILE)gcc

CFLAGS = -Wall -Werror -O3 -fomit-frame-pointer -fno-strict-aliasing

.PHONY:		all clean


all:		amiga-nvram

amiga-nvram:	amiga-nvram.c
		$(CC) $(CFLAGS) -o amiga-nvram amiga-nvram.c

clean:
		$(RM) amiga-nvram

